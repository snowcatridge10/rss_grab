function saveOptions(e) {
    e.preventDefault();
    browser.storage.sync.set({
        feed_reader_url: document.querySelector("#feed_reader_url").value
    });
}

function restoreOptions() {
    function setCurrentChoice(result) {
        document.querySelector("#feed_reader_url").value = result.feed_reader_url || "https://feeds.flossboxin.org.in/p/i/?c=feed&a=add&url_rss=";
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }

    let getting = browser.storage.sync.get("feed_reader_url");
    getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
