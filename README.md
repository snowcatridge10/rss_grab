# RSS Grab

## Install

### From AMO

Head over to https://addons.mozilla.org/en-CA/firefox/addon/rss-grab/ and click install


## What it does

Provides a popup that allows you all the RSS feeds on a page.

## License
GPLv3