document.addEventListener('DOMContentLoaded', function () {
  var optionsBtn = document.getElementById('optionsBtn');

  optionsBtn.addEventListener('click', function () {
    let openingPage = browser.runtime.openOptionsPage();
  });
});

function onError(error) {
  console.log(`Error: ${error}`);
}

function addUrlToFeedList(feed_reader_url, feed_url) {
  var elem = document.createElement("li");
  elem.innerHTML = "<a href='" + feed_reader_url + feed_url + "'>" + feed_url + "</a>";
  document.getElementById("feed_list").appendChild(elem);
}

function onGot(item) {
  browser.tabs.query({ currentWindow: true, active: true }).then((tabs) => {
    let feed_reader_url = "https://feeds.flossboxin.org.in/p/i/?c=feed&a=add&url_rss=";
    if (item.feed_reader_url) {
      feed_reader_url = item.feed_reader_url;
    }

    let tab = tabs[0];

    var myStringArray = ["rss", "feed", "index.xml"];

    for (var i = 0; i < myStringArray.length; i++) {
      if (tab.url.endsWith("/")) {
        feed_url = tab.url + myStringArray[i];
      } else {
        feed_url = tab.url + "/" + myStringArray[i];
      }

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status == 200) {
          if (this.responseText.includes("<rss")) {
            if (document.getElementById("no_feeds") != null) {
              document.getElementById("no_feeds").remove();
            }

            addUrlToFeedList(feed_reader_url, feed_url);
          }
        }
      };
      xmlhttp.open("GET", feed_url, false);
      xmlhttp.send();
    }

    document.getElementById("loading").remove();
  }, console.error)
}

const getting = browser.storage.sync.get("feed_reader_url");
getting.then(onGot, onError);